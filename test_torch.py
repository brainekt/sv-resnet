import torch
import torchaudio
import yaml
import librosa
import numpy as np
import os, tqdm, pickle
from models_torch import ResNetSE34V2
from torchaudio.transforms import MelSpectrogram
from torchaudio.functional import amplitude_to_DB
from metrics import *

def load_audio(file):
    EPS = 1e-8
    s, _ = librosa.load(file, sr=16000)
    amax = np.max(np.abs(s))
    factor = 1.0 / (amax + EPS)
    s = s * factor
    return s


with open('egs/resnet/config.yaml') as f:
    config = yaml.safe_load(f)

sd = torch.load('resnetse34_epoch92.pth')
model = ResNetSE34V2(nOut=256, n_mels=config['fbank']['n_mels'])
model.load_state_dict(sd)
model.eval()
torch.set_grad_enabled(False)

transform = MelSpectrogram(
    sample_rate=config['fbank']['sr'],
    n_fft=config['fbank']['n_fft'],
    win_length=config['fbank']['win_length'],
    hop_length=config['fbank']['hop_length'],
    window_fn=torch.hamming_window,
    n_mels=config['fbank']['n_mels'],
    f_min=config['fbank']['f_min'],
    f_max=config['fbank']['f_max'],
    norm='slaney')

# read scp files
wav_root = 'data'
wav_scp = [a.split()[1].strip() for a in open("data/wav.scp").readlines()]
emb_dict = {}

emb_path = 'emb_dict.pickle'
if os.path.isfile(emb_path) == False:
    for wav in tqdm.tqdm(wav_scp):
        s = load_audio(os.path.join(wav_root, wav))
        x = torch.tensor(s[None, :])
        x = transform(x)
        x = amplitude_to_DB(
            x, multiplier=10, amin=config['fbank']['amin'], db_multiplier=0, top_db=75)

        feature = model(x[:, None, :, :])
        feature = torch.nn.functional.normalize(feature)

        emb_dict[wav] = feature


    with open(emb_path, 'wb') as handle:
        pickle.dump(emb_dict, handle)

with open(emb_path, 'rb') as handle:
    emb_dict = pickle.load(handle)

# build a temp dict
emb_dict_up = {}
for emb in emb_dict:
    emb_dict_up[os.path.basename(emb).replace('.wav', '')] = emb_dict[emb]


# load the test set
test_set = open("data/timit_test.txt").readlines()

labels = []
scores = []

for test in tqdm.tqdm(test_set):
    lab, s1, s2 = [a.strip() for a in test.split()]
    s1 = os.path.basename(s1)
    s2 = os.path.basename(s2)
    w1 = emb_dict_up[s1]
    w2 = emb_dict_up[s2]
    dot = torch.dot(torch.flatten(w1), torch.flatten(w2))

    labels.append(int(lab))
    scores.append(dot)


labels = np.array(labels)
scores = np.array(scores)
print(labels[:5])
print(scores[:5])

result = compute_eer(scores, labels)
print(f"eer: {result.eer} threshold: {result.thresh}")

dcf = compute_min_dcf(result.fr, result.fa)
print(f"min dcf: {dcf}")


# print(os.path.basename(test_set[0].split()[1]))
# print(list(emb_dict.keys())[0].split('\\'))
# print(os.path.basename(list(emb_dict.keys())[0]))